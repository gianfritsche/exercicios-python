# coding=latin-1

# 9. (palinNumero) Faça um programa que descubra se um valor inteiro informado pelo usuário
# e palíndromo, isto e, se lido da esquerda para a direita ou da direita para a esquerda o
# numero e o mesmo.

num=input("informe um valor inteiro: ")

if type(num) is int :
	texto=str(num)
	i=0
	j=len(texto)-1
	while j>i and texto[i] is texto[j]:
		i+=1
		j-=1
	if j <= i:
		print("o valor informado eh palindromo")
	else:
		print("o valor informado NAO eh palindromo")
else:
	print("o valor informado nao eh um inteiro.")