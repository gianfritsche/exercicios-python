# coding=latin-1

# 8. (prnabnt) Escreva programa que leia um nome completo de uma pessoa e imprima na tela o
# último sobrenome com todas as letras maiúsculas, seguido de vírgula, seguido do restante
# do nome, onde apenas o 1o. nome deve estar por extenso e o restante abreviado.
# Ex.: se entrada for ’Armando Luiz Nicolini Delgado’ a saída deve ser ’DELGADO, Armando L. N.’


nome=input("informe o nome completo da pessoa: ")

ultimo=nome.rfind(' ')
saida=(nome[ultimo+1:len(nome)]).upper()
primeiro=nome.find(' ');
saida+=", "+nome[0:primeiro]
pos=nome.find(' ',primeiro)
while pos != ultimo:
	saida+=" "+nome[pos+1]+"."
	pos=nome.find(' ',pos+1)	
print(saida)
