# coding=latin-1

# 1. (achaSpc) Escreva programa que leia um nome completo de uma pessoa e imprima na tela
# a localização de todos os espaços existentes no nome. Se não houver espaços, o programa
# deve indicar isto em uma mensagem conveniente.

nome=input("Informe o nome completo da pessoa: ")

temespaco=False

pos = nome.find(' ')

while pos is not -1:
	print("O caractere", pos, "eh um espaco.")
	temespaco=True
	pos = nome.find(' ', pos+1)

if not temespaco:
	print("Nao foram encontrados espacos no nome.")
