# coding=latin-1

# 11. (calcMedia) Faça programa que obtenha do usuário um conjunto de valores reais e calcule
# a média aritmética dos valores lidos. O usuário indica o final do conjunto de valores
# digitando um caractere não numérico diferente de '.'(ponto).

valor=input("informe um valor real (ou qualquer caractere nao numerico para encerrar): ")

soma=0.0
contador=0

while type(valor) is float :
	soma+=float(valor)
	contador+=1
	valor=input("informe um valor real (ou qualquer caractere nao numerico para encerrar): ")
print("a media dos valores informados eh: ", (soma/float(contador)))
