# coding=latin-1

# 6. (conjPadroes) Faça um programa que obtenha do usuário um conjunto de frases. Para
# cada frase informada, o programa deve solicitar do usuário uma padrão que consiste de
# uma palavra ou conjunto de palavras separadas por espaço a serem encontradas. Quando
# o usuário informar um padrão vazio, o programa pede nova frase para análise. Quando o
# usuário informar uma frase vazia, o programa termina. Para cada padrão, o programa deve
# listar TODAS as posições em que foi encontrado dentro da frase em análise. Caso o padrão
# não ocorra na frase, o programa deve indicar tal fato com uma mensagem adequada.

frase=input("informe uma frase ou uma string vazia para encerrar: ")

while frase is not "":
	padrao=input("informe um conjunto de palavras separadas por espaco ou uma string vazia para sair da fraseuencia atual: ")
	while padrao is not "":
		# procurando pelo padrao na frase
		encontrado=False
		pos=frase.find(padrao)
		while pos is not -1:
			print("PADRAO encontrado na posicao ", pos, "da frase")
			encontrado=True
			pos=frase.find(padrao, pos+1)
		if not encontrado:
			print("o PADRAO nao foi encontrado na frase informada")
		padrao=input("informe um conjunto de palavras separadas por espaco ou uma string vazia para sair da fraseuencia atual: ")
	frase=input("informe a frase ou uma string vazia para encerrar: ")
