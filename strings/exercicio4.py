# coding=latin-1

# 4. (contaSeq) Faça programa que leia conjuntos de 2 sequencias de nucleotídeos e, em cada
# conjunto, indique quantas vezes a 2a. sequencia ocorre na 1a. sequencia. O programa
# termina quando uma das strings for vazia.

seq=input("informe uma sequencia SEQ ou uma string vazia para encerrar: ")
padrao=input("informe uma sequencia PADRAO ou uma string vazia para sair da sequencia atual: ")

while seq is not "" and padrao is not "":
	valido=True
	# validando seq
	i=0
	while i<len(seq) and (seq[i] is 'c' or seq[i] is 't' or seq[i] is 'a' or seq[i] is 'g'):
		i+=1
	if i is not len(seq):
		valido=False
		print("sequencia invalida, a sequencia SEQ deve conter apenas caracteres a, c, t e g.")
	# validando padrao
	i=0
	while i<len(padrao) and (padrao[i] is 'c' or padrao[i] is 't' or padrao[i] is 'a' or padrao[i] is 'g'):
		i+=1
	if i is not len(padrao):
		valido=False
	if i is not len(padrao):
		print("sequencia invalida, a sequencia PADRAO deve conter apenas caracteres a, c, t e g.")
	if valido:
		# procurando pelo padrao na seq
		encontrado=0
		pos=seq.find(padrao)
		while pos is not -1:
			encontrado+=1
			pos=seq.find(padrao, pos+1)
		if encontrado == 0:
			print("o PADRAO nao foi encontrado na sequencia SEQ informada")
		else:
			print("o PADRAO foi encontrado",encontrado," vezes na sequencia SEQ informada")
	seq=input("informe uma sequencia SEQ ou uma string vazia para encerrar: ")
	padrao=input("informe uma sequencia PADRAO ou uma string vazia para sair da sequencia atual: ")
