# coding=latin-1

# 12. (divInt) Escreva um programa que leia dois números e imprima a divisão inteira do primeiro
# pelo segundo, assim como o resto da divisão. Utilize APENAS as operações de SOMA e
# SUBTRAÇÃO para calcular o resultado. Lembre-se que o quociente da divisão de dois
# números é a quantidade inteira de vezes que podemos subtrair o divisor do dividendo. Por
# exemplo, 20 / 4 = 5 pois podemos subtrair 4 cinco vezes de 20.

dividendo=int(input("informe o dividendo: "))
divisor=int(input("informe o divisor: "))

contador=0
aux=dividendo
while aux >= divisor:
	aux-=divisor
	contador+=1
print("a divisao inteira de ", dividendo, "por ", divisor, "eh: ", contador, "com resto:", aux)
