# coding=latin-1

# 2. (achaPadrao) Faça um programa que obtenha do usuário 2 sequências de nucleotídeos (denominadas
# SEQ e PADRAO). O programa deve listar TODAS as posições de PADRAO
# dentro de SEQ. Caso PADRAO não ocorra em SEQ, o programa deve indicar tal fato com
# uma mensagem adequada. O programa deve também testar se as sequencias são válidas,
# isto é, deve testar de nas duas sequências foram indicados apenas nucleotídeos válidos (a,
# c, t, e g).

seq=input("informe a sequencia SEQ: ")
padrao=input("informe a sequencia PADRAO: ")

valido=True

# validando seq
i=0
while i<len(seq) and (seq[i] is 'c' or seq[i] is 't' or seq[i] is 'a' or seq[i] is 'g'):
	i+=1
if i is not len(seq):
	valido=False
	print("sequencia invalida, a sequencia SEQ deve conter apenas caracteres a, c, t e g.")

# validando padrao
i=0
while i<len(padrao) and (padrao[i] is 'c' or padrao[i] is 't' or padrao[i] is 'a' or padrao[i] is 'g'):
	i+=1
if i is not len(padrao):
	valido=False
if i is not len(padrao):
	print("sequencia invalida, a sequencia PADRAO deve conter apenas caracteres a, c, t e g.")

if valido:
	# procurando pelo padrao na seq
	encontrado=False
	pos=seq.find(padrao)
	while pos is not -1:
		print("PADRAO encontrado na posicao ", pos, "da SEQ")
		encontrado=True
		pos=seq.find(padrao, pos+1)

	if not encontrado:
		print("o PADRAO nao foi encontrado na sequencia SEQ informada")
