# coding=latin-1

# 7. (contaprimeira) Faça programa que leia conjuntos de 2 strings e, em cada conjunto, indique
# quantas vezes a 2a. string ocorre na 1a. string. O programa termina quando uma das
# strings for vazia

primeira=input("informe o 1o. conjunto de strings ou uma string vazia para encerrar: ")
segunda=input("informe o 2o. conjunto de strings ou uma string vazia para sair da frase atual: ")

while primeira is not "" and segunda is not "":
	# procurando pela segunda na primeira
	encontrada=0
	pos=primeira.find(segunda)
	while pos is not -1:
		encontrada+=1
		pos=primeira.find(segunda, pos+1)
	if encontrada == 0:
		print("a segunda nao foi encontrada na primeira frase informada")
	else:
		print("a segunda foi encontrada",encontrada," vezes na primeira frase informada")
	primeira=input("informe o 1o. conjunto de strings ou uma string vazia para encerrar: ")
	segunda=input("informe o 2o. conjunto de strings ou uma string vazia para sair da frase atual: ")
