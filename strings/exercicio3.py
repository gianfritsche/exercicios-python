# coding=latin-1

# 3. (conjPadroes) Faça um programa que obtenha do usuário um conjunto de sequências de
# nucleotídeos. Para cada sequencia informada, o programa deve solicitar do usuário um
# conjunto de padrões a serem encontradas. Quando o usuário informar um padrão vazio,
# o programa pede nova sequencia para análise. Quando o usuário informar uma sequencia
# vazia, o programa termina. Para cada padrão, o programa deve listar TODAS as posições
# em que foi encontrado dentro da sequencia em análise. Caso o padrão não ocorra na
# sequencia, o programa deve indicar tal fato com uma mensagem adequada.

seq=input("informe uma sequencia SEQ ou uma string vazia para encerrar: ")

while seq is not "":
	# validando a sequencia SEQ
	valido=True
	i=0
	while i<len(seq) and (seq[i] is 'c' or seq[i] is 't' or seq[i] is 'a' or seq[i] is 'g'):
		i+=1
	if i is not len(seq):
		print("sequencia invalida, a sequencia SEQ deve conter apenas caracteres a, c, t e g.")
	else:
		padrao=input("informe uma sequencia PADRAO ou uma string vazia para sair da sequencia atual: ")
		while padrao is not "":
			# validando a sequencia PADRAO
			valido=True
			i=0
			while i<len(padrao) and (padrao[i] is 'c' or padrao[i] is 't' or padrao[i] is 'a' or padrao[i] is 'g'):
				i+=1
			if i is not len(padrao):
				print("sequencia invalida, a sequencia PADRAO deve conter apenas caracteres a, c, t e g.")
			else:
				# procurando pelo padrao na seq
				encontrado=False
				pos=seq.find(padrao)
				while pos is not -1:
					print("PADRAO encontrado na posicao ", pos, "da SEQ")
					encontrado=True
					pos=seq.find(padrao, pos+1)
				if not encontrado:
					print("o PADRAO nao foi encontrado na sequencia SEQ informada")
			padrao=input("informe uma sequencia PADRAO ou uma string vazia para sair da sequencia atual: ")
		seq=input("informe a sequencia SEQ ou uma string vazia para encerrar: ")
