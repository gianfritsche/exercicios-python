# coding=latin-1

# 5. (achaPadrao) Faça um programa que obtenha do usuário 2 textos (denominadas SEQ e
# PADRAO). O programa deve listar TODAS as posições de PADRAO dentro de SEQ.
# Caso PADRAO não ocorra em SEQ, o programa deve indicar tal fato com uma mensagem
# adequada.

seq=input("informe o texto SEQ: ")
padrao=input("informe o texto PADRAO: ")

encontrado=False
pos=seq.find(padrao)
while pos is not -1:
	print("PADRAO encontrado na posicao ", pos, "da SEQ")
	encontrado=True
	pos=seq.find(padrao, pos+1)

if not encontrado:
	print("o PADRAO nao foi encontrado no texto SEQ informada")
