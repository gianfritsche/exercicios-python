# coding=latin-1

# ** Exercício  3:** 
# crie um programa que realize a leitura de um numero inteiro (N) e de 1 até N imprima:
# a) fizz caso o numero seja múltiplo de 3
# b) buzz caso o numero seja múltiplo de 5
# c) fizz buzz caso o numero seja múltiplo de 3 e 5
# d) caso contrário imprima o numero.

n=int(input("informe um valor inteiro: "))

i=1

while i <= n:
	out=""
	if i%3==0 :
		out="fizz"
	if i%5==0 :
		out+="buzz"
	if i%3 != 0 and i%5 != 0:
		out=str(i)
	print(out)
	i+=1
