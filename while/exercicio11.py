# coding=latin-1

# **Exercício 11:** 
# Construa um programa que efetue a leitura de um número (N) e em seguida imprima a tabuada de N.
# Ex.: 	
# * entrada:
# 5
# * saída:
# 5 x 1 = 5
# 5 x 2 = 10
# ...
# 5 x 9 = 45
# 5 x 10 = 50

n=int(input("informe um numero: "))
i=1
while i<=10:
	print(n, "x", i, "=", (n*i))
	i+=1
