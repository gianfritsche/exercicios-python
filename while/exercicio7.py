# coding=latin-1

# **Exercício 7:**  
# Faça um programa que peça uma nota, entre zero e dez. Mostre uma mensagem caso o valor seja inválido e continue pedindo até que o usuário informe um valor válido.

nota=float(input("informe a nota, entre zero e dez: "))

while nota <0 or nota>10:
	print("nota ", nota, "invalida!")
	nota=float(input("informe a nota, entre zero e dez: "))

print("a nota informada foi: ", nota)
	