# coding=latin-1

# ** Exercício  6:**  
# As Organizações Tabajara resolveram dar um aumento de salario aos seus colaboradores e lhe contrataram para desenvolver um programa que calculara os reajustes. Faça um programa que recebe o salario atual do colaborador e calcule o reajuste de acordo com o seguinte critério:
# * salarios até R$ 280,00 (inclusive): aumento de 20%
# * salarios entre R$ 280,00 e R$ 700,00: aumento de 15%
# * salarios entre R$ 700,00 e R$ 1500,00: aumento de 10%
# * salarios de R$ 1500,00 em diante: aumento de 5%
# Informe na tela:
# * o salario antes do reajuste
# * o percentual de aumento aplicado
# * o valor do aumento
# * o novo salario, após o aumento
# enquanto o salario informado for diferente de zero (0).

salario=float(input("informe o salario atual do colaborador: "))

while salario!=0:

	if salario <= 280.0:
		aumento=0.20
	elif salario <= 700:
		aumento=0.15
	elif salario <= 1500:
		aumento=0.10
	else:
		aumento=0.05
	
	print(" o salario antes do reajuste era de R$", salario)
	print(" sera dado um aumento de", (aumento*100), "%") 
	print(" o valor do aumento sera de R$", (aumento*salario))
	print(" o novo salario do colaborador eh de: R$", (salario+(aumento*salario)))
	
	salario=float(input("informe o salario atual do colaborador: "))
	