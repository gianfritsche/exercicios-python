# coding=latin-1

# ** Exercício 4:**  
# crie um programa que, peça 2 números inteiros e 1 número real, calcule e mostre:
# a) o produto do dobro do primeiro com a metade do segundo.
# b) a soma do triplo do primeiro com o terceiro
# c) o terceiro elevado ao cubo
# enquanto o primeiro número inteiro for diferente de zero (0).

a=int(input("informe o primeiro valor inteiro: "))
b=int(input("informe o segundo valor inteiro: "))
c=float(input("informe um valor real: "))

while a != 0:
	print("o produto do dobro do primeiro com a metade do segundo: ", (2*a)*(b/2.0))
	print("a soma do triplo do primeiro com o terceiro: ", (3*a)+c)
	print("o terceiro elevado ao cubo: {:.3f}" .format(c**3))

	a=int(input("informe o primeiro valor inteiro: "))
	b=int(input("informe o segundo valor inteiro: "))
	c=float(input("informe um valor real: "))