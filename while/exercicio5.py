# coding=latin-1

# ** Exercício  5:**  
# tendo como dados de entrada a altura e o sexo de uma pessoa, construa um programa que calcule seu peso ideal, utilizando as seguintes fórmulas.
# * para homens: (72.7\*h) - 58.0
# * para mulheres: (62.1\*h) - 44.7
# * peça o peso da pessoa e informe se ela está dentro, acima ou abaixo do peso.
# * enquanto a altura (h) for diferente de zero (0).

h=float(input("informe a altura (h) da pessoa (em metros(m)) ou zero para sair: "))

while h!=0:
	sexo=input("informe o sexo da pessoa '[m/f]': ")
	peso=float(input("informe o peso da pessoa (em Quilos (Kg)): "))
	
	if sexo=="m":
		ideal=(72.7*h) - 58.0
	else:
		ideal=(62.1*h) - 44.7

	print("o peso ideal eh: {:.3f}" .format(ideal), "Kg")

	if peso > ideal:
		print("com o peso de {:.3f}" .format(peso), "Kg a pessoa esta acima do peso")
	elif peso < ideal:
		print("com o peso de {:.3f}" .format(peso), "Kg a pessoa esta abaixo do peso")
	else:
		print("com o peso de {:.3f}" .format(peso), "Kg a pessoa esta dentro do peso")

	h=float(input("informe a altura (h) da pessoa (em metros(m)) ou zero para sair: "))
