# coding=latin-1

# **Exercício 9:**  
# Faça um programa que leia e valide as seguintes informações:
# * Nome: maior que 3 caracteres;
# * Idade: entre 0 e 150;
# * Salário: maior que zero;
# * Sexo: 'f' ou 'm';
# * Estado Civil: 's', 'c', 'v', 'd';
# * E ao final imprima as informações lidas e validadas.

nome=input("informe o nome: ")
while len(nome) <= 3:
	print("o nome deve possuir mais de tres caracteres")
	nome=input("informe o nome: ")

idade=int(input("informe a idade: "))
while idade<=0 or idade>=150:
	print("a idade deve ser maior ou igual a zero e menor ou igual a 150")
	idade=int(input("informe a idade: "))

salario=float(input("informe o salario: "))
while salario<=0:
	print("o salario deve ser maior que zero")
	salario=float(input("informe o salario: "))

sexo=input("informe o sexo ['f'/'m']: ")
while sexo != 'f' and sexo != 'm':
	print("o sexo deve ser 'm' ou 'f'")
	sexo=input("informe o sexo ['f'/'m']: ")

estado=input("informe o estado civil ['s'/'c'/'v'/'d']: ")
while estado != 's' and estado != 'c' and estado != 'v' and estado != 'd':
	print("o estado civil deve ser 's', 'c', 'v' ou 'd'")
	estado=input("informe o estado civil ['s'/'c'/'v'/'d']: ")

print("Nome: ", nome)
print("Idade: ", idade)
print("Salario: ", salario)
print("Sexo: ", sexo)
print("Estado civil: ", estado)	
