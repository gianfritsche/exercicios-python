# Lista de Exercícios

### Comando: while (python) ###

** Exercício  1:** crie um programa que realize a leitura de um número inteiro (N) e em seguida, utilizando o laço de repetição “while”, faça a leitura de N números reais e ao final apresente o somatório.

** Exercício  2:**  crie um programa que realize a leitura de um número inteiro (N) e calcule o fatorial do número lido (!N).

** Exercício  3:**  crie um programa que realize a leitura de um número inteiro (N) e de 1 até N imprima:

a) fizz caso o número seja múltiplo de 3

b) buzz caso o número seja múltiplo de 5

c) fizzbuzz caso o número seja múltiplo de 3 e 5

d) caso contrário imprima o número.

** Exercício 4:**  crie um programa que, peça 2 números inteiros e 1 número real, calcule e mostre:

a) o produto do dobro do primeiro com a metade do segundo.

b) a soma do triplo do primeiro com o terceiro

c) o terceiro elevado ao cubo

enquanto o primeiro número inteiro for diferente de zero (0).

** Exercício  5:**  tendo como dados de entrada a altura e o sexo de uma pessoa, construa um programa que calcule seu peso ideal, utilizando as seguintes fórmulas.

* para homens: (72.7\*h) - 58.0

* para mulheres: (62.1\*h) - 44.7
 
* peça o peso da pessoa e informe se ela está dentro, acima ou abaixo do peso.
 
* enquanto a altura (h) for diferente de zero (0).

** Exercício  6:**  As Organizações Tabajara resolveram dar um aumento de salário aos seus colaboradores e lhe contrataram para desenvolver um programa que calculará os reajustes. Faça um programa que recebe o salário atual do colaborador e calcule o reajuste de acordo com o seguinte critério:

* salários até R$ 280,00 (inclusive): aumento de 20%

* salários entre R$ 280,00 e R$ 700,00: aumento de 15%

* salários entre R$ 700,00 e R$ 1500,00: aumento de 10%

* salários de R$ 1500,00 em diante: aumento de 5%

Informe na tela:

* o salário antes do reajuste

* o percentual de aumento aplicado

* o valor do aumento

* o novo salário, após o aumento

enquanto o salário informado for diferente de zero (0).

**Exercício 7:**  Faça um programa que peça uma nota, entre zero e dez. Mostre uma mensagem caso o valor seja inválido e continue pedindo até que o usuário informe um valor válido.

**Exercício 8:**  Faça um programa que leia um nome de usuário e a sua senha e não aceite a senha igual ao nome do usuário, mostrando uma mensagem de erro e voltando a pedir as informações.

**Exercício 9:**  Faça um programa que leia e valide as seguintes informações:

* Nome: maior que 3 caracteres;

* Idade: entre 0 e 150;

* Salário: maior que zero;

* Sexo: 'f' ou 'm';

* Estado Civil: 's', 'c', 'v', 'd';

* E ao final imprima as informações lidas e validadas.

**Exercício 10:**  Construa um programa que efetue a leitura de 5 números e informe qual é o maior.

**Exercício 11:**  Construa um programa que efetue a leitura de um número (N) e em seguida imprima a tabuada de N.

Ex.: 	

* entrada:

5

* saída:

5 x 1 = 5

5 x 2 = 10

…

5 x 9 = 45

5 x 10 = 50
